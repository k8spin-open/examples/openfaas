# OpenFaaS

![Logo](./assets/openfaas.png)

> Serverless Functions, Made Simple.
OpenFaaS® makes it simple to turn anything into a serverless function
that runs on Linux or Windows through Docker Swarm or Kubernetes.

In this repository you will find a quick start guide to try [OpenFaaS](https://www.openfaas.com/) @ [k8spin.cloud](https://k8spin.cloud)

## Disclaimer

**This example is not ready for productive use**. Due to the limitations of the free [k8spin.cloud](https://k8spin.cloud) account during the beta stage, key components have been removed.

## Requirements

You have to have available and configured your namespace configuration file.

```bash
$ pwd
/tmp/openfaas-at-k8spin
$ ls
kubernetes.config
$ export KUBECONFIG=$(pwd)/kubernetes.config
$ kubectl get pods
No resources found.
```

Also, [faas-cli](https://github.com/openfaas/faas-cli/releases) is needed to create, build and deploy functions and a [dockerhub](https://hub.docker.com/) account or any other docker registry is needed to publish the container images to use as functions.

```bash
$ faas-cli version
  ___                   _____           ____
 / _ \ _ __   ___ _ __ |  ___|_ _  __ _/ ___|
| | | | '_ \ / _ \ '_ \| |_ / _` |/ _` \___ \
| |_| | |_) |  __/ | | |  _| (_| | (_| |___) |
 \___/| .__/ \___|_| |_|_|  \__,_|\__,_|____/
      |_|

CLI:
 commit:  2487d1d3443c9b7201c98a2ecc57b617ac58db56
 version: 0.8.10

$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: angelbarrera92
Password:
WARNING! Your password will be stored unencrypted in /home/angel/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

## OpenFaaS deployment

We haven't made [the deployment file](./openfaas.template.yaml) from scratch. This has been partially modified from the output given by the helm chart available in [github](https://github.com/openfaas/faas-netes/tree/master/chart/openfaas).

In this repository you can find the [variable file](./develop/values.yaml) used to render the [helm chart](https://github.com/openfaas/faas-netes/tree/master/chart/openfaas). It can serve as a guide if you want to try to do the same work that we have already done.

```bash
$ pwd
/tmp/openfaas-at-k8spin
$ git clone https://github.com/openfaas/faas-netes.git
$ git clone https://gitlab.com/k8spin-open/examples/openfaas.git
$ helm template faas-netes/chart/openfaas \
--name openfaas \
--namespace angelbarrerasanchez-gmail-com-openfaas \
--values openfaas/develop/values.yaml > $(pwd)/openfaas.yaml
```

### Requirements

We have to protect the OpenFaaS web interface. To do this we have to create a secret in kubernetes with a password:

```bash
PASSWORD=$(head -c 12 /dev/urandom | shasum| cut -d' ' -f1)
$ kubectl create secret generic basic-auth --from-literal=basic-auth-user=admin  --from-literal=basic-auth-password="${PASSWORD}" --dry-run -o yaml | kubectl apply -f -
secret/basic-auth created
```

Then open the [template file](./openfaas.template.yaml) *(located in `/tmp/openfaas-at-k8spin/openfaas/openfaas.template.yaml`)* and replace the following placeholders *(you can use any text editor)*:

- **`${NAMESPACE_NAME}`**: Indicates the namespace where OpenFaaS will be deployed. You can find it in the [k8spin.cloud](https://k8spin.cloud) console.
- **`${SERVICE_ACCOUNT_NAME}`**: `serviceaccount` used by OpenFaaS controller. Actually, [k8spin.cloud](https://k8spin.cloud) *(beta stage)* provides only one serviceaccount wich name is the same as the namespace name.
- **`${CERTMANAGER_ISSUER}`**: certmanager issuer. As OpenFaaS gateway is being exposed as an https ingress, a certificate has to be requested. Put the value returned by `kubectl get issuer`.
- **`${GATEWAY_HOST}`**: Public dns to expose the OpenFaaS gateway. This one has to meet the wildcard assigned to your namespace. The value of this wildcard is accessible on the [k8spin.cloud](https://k8spin.cloud) web console. Having a wildcard: `*.angelbarrerasanchez.apps.beta.k8spin.cloud` you can use: `gateway.openfaas.angelbarrerasanchez.apps.beta.k8spin.cloud`.

### Deploy OpenFaaS

Once you have all the placeholders replaced with the correct values, you will be able to deploy it into your namespace.

```bash
$ pwd
/tmp/openfaas-at-k8spin
$ cd openfaas
$ kubectl apply -f ./openfaas.template.yaml
service/gateway-external created
service/gateway created
deployment.apps/gateway created
ingress.extensions/openfaas-ingress created
```

After a few moments you should see a pod with two containers:

```bash
$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
gateway-79b46fc4fb-htjww   2/2     Running   0          51s
```

It' s almost done. Access the OpenFaaS cli with the public url of the gateway, the admin user and the password that we have created previously.

```bash
$ faas-cli login -g https://gateway.openfaas.angelbarrerasanchez.apps.beta.k8spin.cloud -u admin -p ${PASSWORD}
WARNING! Using --password is insecure, consider using: cat ~/faas_pass.txt | faas-cli login -u user --password-stdin
Calling the OpenFaaS server to validate the credentials...
credentials saved for admin https://gateway.openfaas.angelbarrerasanchez.apps.beta.k8spin.cloud
```

### Deploy a Function

Before deploying the function, let's take a look at the function configuration file:

*open the [`example/hello-go.yml`](./example/hello-go.yml) file*
```yaml
provider:
  name: openfaas
  gateway: https://${GATEWAY_HOST}
functions:
  hello-go:
    lang: go
    handler: ./hello-go
    image: ${DOCKERHUB_USER}/hello-go:latest
    limits:
      cpu: 5m
      memory: 80Mi
    requests:
      cpu: 5m
      memory: 80Mi
```

Again, you'll have to replace some values:

- **`${GATEWAY_HOST}`**: The same as previously configured
- **`${DOCKERHUB_USER}`**: The dockerhub username.

Once the values have been substituted, we can build and deploy the function:

```bash
$ pwd
/tmp/openfaas-at-k8spin/openfaas
$ cd ./example
$ faas-cli build -f ./hello-go.yml
[0] > Building hello-go.
Clearing temporary build folder: ./build/hello-go/
Preparing ./hello-go/ ./build/hello-go//function
Building: angelbarrera92/hello-go:latest with go template. Please wait..
Sending build context to Docker daemon  6.656kB
Step 1/22 : FROM golang:1.10.4-alpine3.8 as builder
1.10.4-alpine3.8: Pulling from library/golang
4fe2ade4980c: Pull complete
2e793f0ebe8a: Pull complete
77995fba1918: Pull complete
4495499e856d: Pull complete
0ff8f8e34aa6: Pull complete
Digest: sha256:55ff778715a4f37ef0f2f7568752c696906f55602be0e052cbe147becb65dca3
Status: Downloaded newer image for golang:1.10.4-alpine3.8
 ---> bd36346540f3
Step 2/22 : ARG ADDITIONAL_PACKAGE
 ---> Running in 3d946e346c75
Removing intermediate container 3d946e346c75
 ---> 3c6ceaccd9c7
Step 3/22 : ARG CGO_ENABLED=0
 ---> Running in e5a0b76ed5c2
Removing intermediate container e5a0b76ed5c2
 ---> 7600c2404e28
Step 4/22 : RUN apk --no-cache add curl ${ADDITIONAL_PACKAGE}     && echo "Pulling watchdog binary from Github."     && curl -sSL https://github.com/openfaas/faas/releases/download/0.13.0/fwatchdog > /usr/bin/fwatchdog     && chmod +x /usr/bin/fwatchdog     && apk del curl --no-cache
 ---> Running in 270b67ffd3f5
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
(1/4) Installing nghttp2-libs (1.32.0-r0)
(2/4) Installing libssh2 (1.8.2-r0)
(3/4) Installing libcurl (7.61.1-r2)
(4/4) Installing curl (7.61.1-r2)
Executing busybox-1.28.4-r1.trigger
OK: 6 MiB in 18 packages
Pulling watchdog binary from Github.
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
(1/4) Purging curl (7.61.1-r2)
(2/4) Purging libcurl (7.61.1-r2)
(3/4) Purging nghttp2-libs (1.32.0-r0)
(4/4) Purging libssh2 (1.8.2-r0)
Executing busybox-1.28.4-r1.trigger
OK: 5 MiB in 14 packages
Removing intermediate container 270b67ffd3f5
 ---> 22e722fc07ec
Step 5/22 : WORKDIR /go/src/handler
 ---> Running in 51de2d704c81
Removing intermediate container 51de2d704c81
 ---> bade352156bf
Step 6/22 : COPY . .
 ---> a8304bd9adaf
Step 7/22 : RUN test -z "$(gofmt -l $(find . -type f -name '*.go' -not -path "./vendor/*" -not -path "./function/vendor/*"))" || { echo "Run \"gofmt -s -w\" on your Golang code"; exit 1; }
 ---> Running in f2995f5d1a3d
Removing intermediate container f2995f5d1a3d
 ---> 898430df013d
Step 8/22 : RUN CGO_ENABLED=${CGO_ENABLED} GOOS=linux     go build --ldflags "-s -w" -a -installsuffix cgo -o handler . &&     go test $(go list ./... | grep -v /vendor/) -cover
 ---> Running in ff55c3040719
?       handler [no test files]
?       handler/function        [no test files]
Removing intermediate container ff55c3040719
 ---> f88f9d77987c
Step 9/22 : FROM alpine:3.8
3.8: Pulling from library/alpine
c87736221ed0: Pull complete
Digest: sha256:a4d41fa0d6bb5b1194189bab4234b1f2abfabb4728bda295f5c53d89766aa046
Status: Downloaded newer image for alpine:3.8
 ---> dac705114996
Step 10/22 : RUN apk --no-cache add ca-certificates
 ---> Running in 9abe5c9853b9
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
(1/1) Installing ca-certificates (20171114-r3)
Executing busybox-1.28.4-r3.trigger
Executing ca-certificates-20171114-r3.trigger
OK: 5 MiB in 14 packages
Removing intermediate container 9abe5c9853b9
 ---> 86b0ef2ca96a
Step 11/22 : RUN addgroup -S app && adduser -S -g app app
 ---> Running in a7a2fea9600e
Removing intermediate container a7a2fea9600e
 ---> 1acf31192ae2
Step 12/22 : RUN mkdir -p /home/app
 ---> Running in 21f4e071f098
Removing intermediate container 21f4e071f098
 ---> f32a4c2feb3a
Step 13/22 : WORKDIR /home/app
 ---> Running in 84a13c46877e
Removing intermediate container 84a13c46877e
 ---> cc78906d5b22
Step 14/22 : COPY --from=builder /usr/bin/fwatchdog         .
 ---> f1ab43531b98
Step 15/22 : COPY --from=builder /go/src/handler/function/  .
 ---> 3e3d580232b1
Step 16/22 : COPY --from=builder /go/src/handler/handler    .
 ---> ab58f1d6b746
Step 17/22 : RUN chown -R app /home/app
 ---> Running in 851b55336234
Removing intermediate container 851b55336234
 ---> 28964bf1ed20
Step 18/22 : USER app
 ---> Running in 8137a873faa5
Removing intermediate container 8137a873faa5
 ---> d2acb9875550
Step 19/22 : ENV fprocess="./handler"
 ---> Running in d0893598b978
Removing intermediate container d0893598b978
 ---> 1f45af96725b
Step 20/22 : EXPOSE 8080
 ---> Running in 3bcd7dca5afb
Removing intermediate container 3bcd7dca5afb
 ---> c9f63fdcb964
Step 21/22 : HEALTHCHECK --interval=3s CMD [ -e /tmp/.lock ] || exit 1
 ---> Running in 66bef8eaf528
Removing intermediate container 66bef8eaf528
 ---> 9a9441cc90fa
Step 22/22 : CMD ["./fwatchdog"]
 ---> Running in 47dfe4f9c67c
Removing intermediate container 47dfe4f9c67c
 ---> ef2a8925b679
Successfully built ef2a8925b679
Successfully tagged angelbarrera92/hello-go:latest
Image: angelbarrera92/hello-go:latest built.
[0] < Building hello-go done.
[0] worker done.

$ faas-cli push -f ./hello-go.yml
[0] > Pushing hello-go [angelbarrera92/hello-go:latest].
The push refers to repository [docker.io/angelbarrera92/hello-go]
53b1db0ceac7: Pushed
a5a37ded09c5: Pushed
772492729603: Pushed
94736e0e37e0: Pushed
0ca442f1b05b: Pushed
68577fcb301c: Pushed
d9ff549177a9: Mounted from library/alpine
latest: digest: sha256:c2a73504e446f23ae7e5171046c6738e3a5b0a1b7c6cdbd9bc5c360a42592c07 size: 1785
[0] < Pushing hello-go [angelbarrera92/hello-go:latest] done.
[0] worker done.

$ faas-cli deploy -f ./hello-go.yml
Deploying: hello-go.

Deployed. 202 Accepted.
URL: https://gateway.openfaas.angelbarrerasanchez.apps.beta.k8spin.cloud/function/hello-go
```

### Test it

```bash
$ curl https://gateway.openfaas.angelbarrerasanchez.apps.beta.k8spin.cloud/function/hello-go -d "K8Spin.cloud rocks"
Hello, Go. You said: K8Spin.cloud rocks
```

## Extra work

To make OpenFaaS work in an environment of limited resources and permissions, we have opted for a strategy of avoiding components. The following components have been omitted:

- `async`. provided by Nats
- `operator`. As we have only access to a single namespace, we can not deploy CRDs (cluster scope).
- `prometheus`. This is so heavy to run in a limited resource namespace.
- `alertmanager`. Deployed along with `prometheus`.
- `faasIdler`. As a POC it is not neccessary to have this piece deployed.

Security work has also been done making all containers *(not just one)* run as a non-root user.
Resources have been optimized by setting limits on containers.
About RBAC, being limited by [k8spin.cloud](https://k8spin.cloud) with only one serviceaccount, it has been set for the OpenFaaS deployment instead of the one proposed by the helm's chart.

